#include <iostream>
#include <chrono>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <mpi.h>
#include <time.h>
#include <H5Cpp.h>
#include "svm.h"

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define Calloc(type,n) (type *)calloc(n, sizeof(type))

void exit_with_help()
{
    printf(
        "Usage: svm-train [options] training_set_file [model_file]\n"
        "options:\n"
        "-s svm_type : set type of SVM (default 0)\n"
        "	0 -- C-SVC\n"
        "	1 -- nu-SVC\n"
        "	2 -- one-class SVM\n"
        "	3 -- epsilon-SVR\n"
        "	4 -- nu-SVR\n"
        "-t kernel_type : set type of kernel function (default 2)\n"
        "	0 -- linear: u'*v\n"
        "	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
        "	2 -- radial basis function: exp(-gamma*|u-v|^2)\n"
        "	3 -- sigmoid: tanh(gamma*u'*v + coef0)\n"
        "-d degree : set degree in kernel function (default 3)\n"
        "-g gamma : set gamma in kernel function (default 1/k)\n"
        "-r coef0 : set coef0 in kernel function (default 0)\n"
        "-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)\n"
        "-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)\n"
        "-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)\n"
        "-m cachesize : set cache memory size in MB (default 40)\n"
        "-e epsilon : set tolerance of termination criterion (default 0.001)\n"
        "-h shrinking: whether to use the shrinking heuristics, 0 or 1 (default 1)\n"
        "-b probability_estimates: whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)\n"
        "-wi weight: set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
        "-v n: n-fold cross validation mode\n"
        "-o n: max. size of working set\n"
        "-q n: max. number of new variables entering working set\n"
        "-f format : set the format type of the input file\n"
        "   0 -- File is in svmlight format\n"
        "   1 -- File is in HDF5 format as formatted by pisvm-parse, can be combined with -D to read HDF5 data in dense format\n"
        "flags:\n"
        "-D: Assume the feature vectors are dense (default: sparse)\n"
    );
    exit(1);
}

void parse_command_line(int argc, char **argv, char *input_file_name, char *model_file_name);
void read_problem(const char *filename);
void read_problem_dense(const char *filename);
int read_problem_hdf5(const char *filename, bool denseformat);
int svm_save_model_hdf5(const char *model_file_name, const svm_model *model);
void do_cross_validation();

struct svm_parameter param;		// set by parse_command_line
struct svm_problem prob;		// set by read_problem
struct svm_model *model;

Xfloat *x_space;
int *nz_idx_space;
int cross_validation;
int format;
int dense_features;
int nr_fold;

//dataset names used in the HDF5 file
const H5std_string  CLASSSET_NAME("class"); //double
const H5std_string  FEATURESET_NAME("feature_value"); //float
const H5std_string  DENSITYMAP_NAME("densitymap"); //bool


int main(int argc, char **argv)
{
    //Start recoding the time when execution starts
	std::chrono::high_resolution_clock::time_point execution_begin = std::chrono::high_resolution_clock::now();
    
    char input_file_name[1024];
    char model_file_name[1024];
    const char *error_msg;
    
    MPI_Init(&argc, &argv);
    
    parse_command_line(argc, argv, input_file_name, model_file_name);
    
    //Start recording the Input time
    std::chrono::high_resolution_clock::time_point input_begin = std::chrono::high_resolution_clock::now();
    
    if (format == 0) //use svmlight format
    {
        if (dense_features)
            read_problem_dense(input_file_name);
        else
            read_problem(input_file_name);
    }
    else if (format == 1) //use hdf5 format
    {
        int retval = read_problem_hdf5(input_file_name, dense_features); //same function reads both sparse and dense data
        if (retval != 0)
        {
            fprintf(stderr, "Problem reading HDF5 file\n");
            return 0; //stop execution, everything is deallocated on program exit
        }
    }
    
    //Stop recording the Input time
    std::chrono::high_resolution_clock::time_point input_end = std::chrono::high_resolution_clock::now();
    
    //Start recording the Processing time
    std::chrono::high_resolution_clock::time_point processing_begin = std::chrono::high_resolution_clock::now();
    
    error_msg = svm_check_parameter(&prob,&param);
    if(error_msg)
    {
        fprintf(stderr,"Error: %s\n",error_msg);
        exit(1);
    }
    
	int rank = -1;
    if(cross_validation)
    {
        do_cross_validation();
    }
    else
    {
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        model = svm_train(&prob,&param);

        if (rank == 0)
        {
            //Stop recording the Processing time
            std::chrono::high_resolution_clock::time_point processing_end = std::chrono::high_resolution_clock::now();
            
            //Start recording the Output time
            std::chrono::high_resolution_clock::time_point output_begin = std::chrono::high_resolution_clock::now();
            if (format == 0) //Save model in svmlight format
            {
                svm_save_model(model_file_name,model);
            }
            else if (format == 1) //Save mode in HDF5 format
            {
                model->max_idx = prob.max_idx; //the model needs max_idx set in order to set dataset dimensions
                int retval = svm_save_model_hdf5(model_file_name, model);
            }
            std::chrono::high_resolution_clock::time_point output_end = std::chrono::high_resolution_clock::now();
    
            //Write out the Input time
            int inputtime = std::chrono::duration_cast<std::chrono::milliseconds>(input_end - input_begin).count();
            fprintf(stdout, "Input time: %d\n", inputtime);
            
            //Write out the Processing time
            int processingtime = std::chrono::duration_cast<std::chrono::milliseconds>(processing_end - processing_begin).count();
            fprintf(stdout, "Processing time: %d\n", processingtime);
            
            //Write out the Output time
            int outputtime = std::chrono::duration_cast<std::chrono::milliseconds>(output_end - output_begin).count();
            fprintf(stdout, "Output time: %d\n", outputtime);
        }
        
        svm_destroy_model(model);
    }
    svm_destroy_param(&param);
    free(prob.y);
    free(prob.x);
    free(prob.nz_idx);
    free(prob.x_len);
    free(x_space);
    free(nz_idx_space);

    //Write out the total execution time
    if(rank == 0)
    {
        std::chrono::high_resolution_clock::time_point execution_end = std::chrono::high_resolution_clock::now();
	    int executiontime = std::chrono::duration_cast<std::chrono::milliseconds>(execution_end - execution_begin).count();
	    fprintf(stdout, "Execution time: %d\n", executiontime);
    }

    MPI_Finalize(); //needs to be after timing so rank can be used.

    return 0;
}


void do_cross_validation()
{
    int i;
    int total_correct = 0;
    double total_error = 0;
    double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
    double *target = Malloc(double,prob.l);

    svm_cross_validation(&prob,&param,nr_fold,target);
    if(param.svm_type == EPSILON_SVR ||
            param.svm_type == NU_SVR)
    {
        for(i=0; i<prob.l; i++)
        {
            double y = prob.y[i];
            double v = target[i];
            total_error += (v-y)*(v-y);
            sumv += v;
            sumy += y;
            sumvv += v*v;
            sumyy += y*y;
            sumvy += v*y;
        }
        printf("Cross Validation Mean squared error = %g\n",total_error/prob.l);
        printf("Cross Validation Squared correlation coefficient = %g\n",
               ((prob.l*sumvy-sumv*sumy)*(prob.l*sumvy-sumv*sumy))/
               ((prob.l*sumvv-sumv*sumv)*(prob.l*sumyy-sumy*sumy))
              );
    }
    else
    {
        for(i=0; i<prob.l; i++)
            if(target[i] == prob.y[i])
                ++total_correct;
        printf("Cross Validation Accuracy = %g%%\n",100.0*total_correct/prob.l);
    }
    free(target);
}


void parse_command_line(int argc, char **argv, char *input_file_name, char *model_file_name)
{
    int i;

    // default values
    param.svm_type = C_SVC;
    param.kernel_type = RBF;
    param.degree = 3;
    param.gamma = 0;	// 1/k
    param.coef0 = 0;
    param.nu = 0.5;
    param.cache_size = 40;
    param.C = 1;
    param.eps = 1e-3;
    param.p = 0.1;
    param.shrinking = 1;
    param.probability = 0;
    param.nr_weight = 0;
    param.weight_label = NULL;
    param.weight = NULL;
    param.o = 2; // safe defaults
    param.q = 2;
    cross_validation = 0;
    dense_features = 0;

    // parse options
    for(i=1; i<argc; i++)
    {
        if(argv[i][0] != '-') break;
        if(++i>=argc)
            exit_with_help();
        switch(argv[i-1][1])
        {
        case 'o':
            param.o = atoi(argv[i]);
            break;
        case 'q':
            param.q = atoi(argv[i]);
            break;
        case 's':
            param.svm_type = atoi(argv[i]);
            break;
        case 't':
            param.kernel_type = atoi(argv[i]);
            if (param.kernel_type < 0) {
                fprintf(stderr,"Invalid kernel type\n");
                exit_with_help();
            }
            break;
        case 'd':
            param.degree = atoi(argv[i]);
            break;
        case 'g':
            param.gamma = atof(argv[i]);
            break;
        case 'r':
            param.coef0 = atof(argv[i]);
            break;
        case 'n':
            param.nu = atof(argv[i]);
            break;
        case 'm':
            param.cache_size = atof(argv[i]);
            break;
        case 'c':
            param.C = atof(argv[i]);
            break;
        case 'e':
            param.eps = atof(argv[i]);
            break;
        case 'p':
            param.p = atof(argv[i]);
            break;
        case 'h':
            param.shrinking = atoi(argv[i]);
            break;
        case 'b':
            param.probability = atoi(argv[i]);
            break;
        case 'f':
            format = atoi(argv[i]);
            break;
        case 'v':
            cross_validation = 1;
            nr_fold = atoi(argv[i]);
            if(nr_fold < 2)
            {
                fprintf(stderr,"n-fold cross validation: n must >= 2\n");
                exit_with_help();
            }
            break;
        case 'w':
            ++param.nr_weight;
            param.weight_label =
                (int *)realloc(param.weight_label,sizeof(int)*param.nr_weight);
            param.weight =
                (double *)realloc(param.weight,sizeof(double)*param.nr_weight);
            param.weight_label[param.nr_weight-1] = atoi(&argv[i-1][2]);
            param.weight[param.nr_weight-1] = atof(argv[i]);
            break;
        case 'D':
            dense_features = 1;
            i--;
            break;
        default:
            fprintf(stderr,"unknown option\n");
            exit_with_help();
        }
    }

    // determine filenames

    if (dense_features) {
        param.kernel_type = -(param.kernel_type + 1);
    }
    if(i>=argc)
        exit_with_help();

    strcpy(input_file_name, argv[i]);

    if(i == argc - 2) //There is exactly 1 additional parameter after the input file name
        strcpy(model_file_name,argv[i+1]);
    else if (i == argc - 1)//input file name is the last parameter
    {
        char *p = strrchr(argv[i],'/');
        if(p==NULL)
            p = argv[i];
        else
            ++p;
        sprintf(model_file_name,"%s.model",p);
    } else { //There are more parameters
        printf("ERROR: There are unparsed parameters!\n");
        exit_with_help();
    }
}


// read in a problem (in svmlight format)
void read_problem(const char *filename)
{
    int elements, i, j;
    FILE *fp = fopen(filename,"r");

    if(fp == NULL)
    {
        fprintf(stderr,"can't open input file %s\n",filename);
        exit(1);
    }

    prob.l = 0;
    elements = 0;
    while(1)
    {
        int c = fgetc(fp);
        switch(c)
        {
        case '\n':
            ++prob.l;
            break;
        case ':':
            ++elements;
            break;
        case EOF:
            goto out;
        default:
            ;
        }
    }
out:
    rewind(fp);
    prob.y = Malloc(double,prob.l);
    prob.x = Malloc(Xfloat *, prob.l);
    prob.nz_idx = Malloc(int *, prob.l);
    prob.x_len = Malloc(int, prob.l);
    //TODO: Check if not needed - loop sets prob.x_len[i] = 0 for i=0 to prob.l
    memset(prob.x_len, 0, sizeof(int)*prob.l);
    x_space = Malloc(Xfloat,elements);
    nz_idx_space = Malloc(int,elements);

    prob.max_idx = 0;
    j=0;
    for(i=0; i<prob.l; i++)
    {
        double label;
        prob.x[i] = &x_space[j];
        prob.nz_idx[i] = &nz_idx_space[j];
        prob.x_len[i] = 0;
        fscanf(fp,"%lf",&label);
        prob.y[i] = label;
        while(1)
        {
            int c;
            do {
                c = getc(fp);
                if(c=='\n') goto out2;
            } while(isspace(c));
            ungetc(c,fp);
            //	  fscanf(fp,"%d:%lf",&nz_idx_space[j],&x_space[j]);
            fscanf(fp,"%d:%f",&nz_idx_space[j],&x_space[j]);
	    if (nz_idx_space[j] == 0) {
		    fprintf(stderr, "ERROR: Feature indices need to be 1-based!\n");
		    exit(1);
	    }
            --nz_idx_space[j]; // we need zero based indices
            ++prob.x_len[i];
            ++j;
        }
out2:
        if(j>=1 && nz_idx_space[j-1]+1 > prob.max_idx)
        {
            prob.max_idx = nz_idx_space[j-1]+1;
        }
    }
    if(param.gamma == 0)
        param.gamma = 1.0/prob.max_idx;

    {
        float feature_density = 100.0*elements/(prob.l*prob.max_idx);
        if (feature_density > 50) {
            printf("The features from the model file have a density of %.2f%%. \n"
                   "You %s consider using the -D flag to use a dense feature representation.\n",
                   feature_density, feature_density > 75 ? (feature_density > 90 ? "SHOULD" : "should") : "might");
        }
    }

    fclose(fp);
}


void read_problem_dense(const char *filename)
{
    int i, j;
    FILE *fp = fopen(filename,"r");

    if(fp == NULL)
    {
        fprintf(stderr,"can't open input file %s\n",filename);
        exit(1);
    }

    prob.l = 0;
    prob.max_idx = 0;
    while(1)
    {
        if (fscanf(fp, "%*f") == EOF) break;
        while (1) {
            int c;
            do {
                c = getc(fp);
            } while(isspace(c) && c != '\n');
            if (c == '\n') {
                break;
            } else if (c == EOF) {
                goto out;
            } else {
                ungetc(c,fp);
                int idx;
                int ret = fscanf(fp, "%d:%*f", &idx);
                if (ret == EOF) {
                    goto out;
                }
                if (idx > prob.max_idx) prob.max_idx = idx;
            }
        }
        prob.l ++;
    }
out:
    rewind(fp);
    prob.y = Malloc(double,prob.l);
    prob.x = Malloc(Xfloat *, prob.l);
    prob.nz_idx = Malloc(int *, prob.l);
    prob.x_len = Malloc(int, prob.l);
    x_space = Malloc(Xfloat,prob.l*prob.max_idx);
    nz_idx_space = Malloc(int,prob.max_idx);
    for (i=0; i < prob.max_idx;i++) {
        nz_idx_space[i] = i;
    }

    //prob.max_idx = 0;
    //j=0;
    for(i=0; i<prob.l; i++)
    {
        double label;
        prob.x[i] = &x_space[prob.max_idx*i];
        prob.nz_idx[i] = nz_idx_space;
        prob.x_len[i] = prob.max_idx;
        fscanf(fp,"%lf",&label);
        prob.y[i] = label;
        j = 0;
        while(1)
        {
            int c, idx;
            Xfloat value;
            do {
                c = getc(fp);
            } while(isspace(c) && c != '\n');
            if (c == '\n') break;
            ungetc(c,fp);
            //	  fscanf(fp,"%d:%lf",&nz_idx_space[j],&x_space[j]);
            fscanf(fp,"%d:%f",&idx,&value);
	    if (idx == 0) {
		fprintf(stderr, "ERROR: Feature indices need to be 1-based!\n");
		exit(1);
	    }
            idx -= 1; // we need zero based indices
            for (;j < idx; j++) {
                prob.x[i][j] = 0;
            }
            j = idx +1;
            prob.x[i][idx] = value;
        }
    }
    if(param.gamma == 0)
        param.gamma = 1.0/prob.max_idx;

    fclose(fp);
}


int read_problem_hdf5(const char *filename, bool denseformat)
{
    try
    {
        H5::Exception::dontPrint();
        H5::H5File file(filename, H5F_ACC_RDONLY ); //read access only
        
        //we open the three datasets from the file
        H5::DataSet dsclass = file.openDataSet( CLASSSET_NAME );
        H5::DataSet dsfeatureset = file.openDataSet( FEATURESET_NAME );
        H5::DataSet dsdensitymap = file.openDataSet( DENSITYMAP_NAME );
        
        //we need to read the feature count, the value depends on if we want dense data or not, that value is used to allocate space for the data we read
        int featurecount = 0;
        H5::Attribute attr_featurecount = denseformat == true ? file.openAttribute("DenseFeatureCount") : file.openAttribute("SparseFeatureCount");
        H5::DataType type = attr_featurecount.getDataType(); //Extract the datatype
        attr_featurecount.read(type, &featurecount); //Read the value
        
        //we will need two dataspaces, since the dataspace for dsfeatureset and dsdensitymap is the same
        H5::DataSpace dim1 = dsclass.getSpace();
        H5::DataSpace dim2 = dsfeatureset.getSpace();
        
        //extract the amount of records(lines) and features(columns) in dsfeatureset
        hsize_t dims_out[2];
        int ndims = dim2.getSimpleExtentDims(dims_out, NULL);
        int line_count = (int)dims_out[0];
        int max_feature_id = (int)dims_out[1];
        
        //These arrays must be allocated on the heap because of the size, they also have to be that big because that is how we read them from the file
		//we must then loop through then and put the data into nz_idx_space and x_space
        double *class_dataset = Malloc(double, line_count); //we initialize an array to hold the class value
        float *feature_dataset = Malloc(float, line_count * max_feature_id); //an array to hold all values
        bool *densitymap_dataset = Malloc(bool, line_count * max_feature_id); //We initialize an array to hold bool values that tell us if the value was found in the file or if it is a filler to create dense data
        
        //we now read the dataset
        dsclass.read(class_dataset, H5::PredType::NATIVE_DOUBLE, dim1);
        dsfeatureset.read(feature_dataset, H5::PredType::NATIVE_FLOAT, dim2);
        dsdensitymap.read(densitymap_dataset, H5::PredType::NATIVE_UCHAR, dim2);
        
        //start initializing the svm_problem values and populating them
		prob.l = line_count;
		prob.y = Malloc(double,line_count); //the label/class
		prob.x = Malloc(Xfloat *, line_count); //feature value
		prob.nz_idx = Malloc(int *, line_count); //feature indexes
		prob.x_len = Malloc(int, line_count); //feature count in each line
		prob.max_idx = max_feature_id; //highest index found while parsing the file
        
        //intermediate structs to store the feature index and feature values in each line
        Xfloat *x_space = Malloc(Xfloat, featurecount);
		int *nz_idx_space = Malloc(int, featurecount);
        
        int j = 0; //j will keep track of where we add items into x_space and nz_idx_space
        for (int i = 0; i < line_count; i++)
        {
            //We store the label
            prob.y[i] = class_dataset[i];
            
            prob.nz_idx[i] = &nz_idx_space[j]; //and we store a pointer to the first featureid
            prob.x[i] = &x_space[j]; //we store a pointer to the first value
            
            prob.x_len[i] = 0;
            for (int e = 0; e < max_feature_id; e++)
            {
                int isindataset = densitymap_dataset[i * max_feature_id + e];
                if ((bool)isindataset == true || denseformat == true) //this value was in the dataset
                {
                    nz_idx_space[j] = e ; //e is the feature_ID, we want it zero-based just like it is stored in HDF5
                    x_space[j] = feature_dataset[i * max_feature_id + e];
                    j++; //we found a feature:value, increment j
                    prob.x_len[i]++;
                }
            }
        }
        
        //Do some cleanup
        free(class_dataset);
        free(feature_dataset);
        free(densitymap_dataset);
        
        //Great success!
        return 0;
    }
    // catch generic H5 exception
    catch(H5::Exception ex)
    {
        ex.printError(stderr); //print the error
    }
    return -1;
}


int svm_save_model_hdf5(const char *model_file_name, const svm_model *model)
{
    //Fetch the parameters from the model
    const svm_parameter &param = model->param;
    
    //Don't save dense kernel type to file
    int kernel_type = param.kernel_type;
    int is_dense = kernel_type < 0;
    if (is_dense)
        kernel_type = -kernel_type - 1;
   
    try
    {
		//Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
		H5::Exception::dontPrint();
		
		//create the file
		H5::H5File file(model_file_name, H5F_ACC_TRUNC);
        
        //create a new dataspace for attributes
        H5::DataSpace attr_dataspace(H5S_SCALAR);
        
        //define datatypes
        H5::IntType int_type(H5::PredType::NATIVE_INT); //define a type for INTs
        H5::FloatType float_type(H5::PredType::NATIVE_FLOAT); //define a type for Floats
        H5::PredType double_type(H5::PredType::NATIVE_DOUBLE); //define the datatype for class_dataset, H5T_NATIVE_DOUBLE
        H5::StrType str_type(H5::PredType::C_S1, 20); //define a type for strings, TODO, when size is set to H5T_VARIABLE it causes segmentation fault, figure out why, until then 20 should be enough for everyone

        //Store svm_type, %s
        H5::Attribute attr_svm_type = file.createAttribute("svm_type", str_type, attr_dataspace);
        attr_svm_type.write(str_type, svm_type_table[param.svm_type]);
        attr_svm_type.close();

        //Store kernel_type, %s
        H5::Attribute attr_kernel_type = file.createAttribute("kernel_type", str_type, attr_dataspace);
        attr_kernel_type.write(str_type, kernel_type_table[kernel_type]);
        attr_kernel_type.close();

        if(kernel_type == POLY)
        {
            //Store degree, %d
            H5::Attribute attr_degree = file.createAttribute("degree", int_type, attr_dataspace);
            attr_degree.write(int_type, &param.degree);
            attr_degree.close();
        }
        
        if(kernel_type == POLY || kernel_type == RBF || kernel_type == SIGMOID)
        {
            //Store gamma, %g
            H5::Attribute attr_gamma = file.createAttribute("gamma", double_type, attr_dataspace);
            attr_gamma.write(double_type, &param.gamma);
            attr_gamma.close();
        }
        
        if(kernel_type == POLY || kernel_type == SIGMOID)
        {
            //Store coef0, %g
            H5::Attribute attr_coef0 = file.createAttribute("coef0", double_type, attr_dataspace);
            attr_coef0.write(double_type,  &param.coef0);
            attr_coef0.close();
        }
        
        //Store nr_class, number of classes, %d
        int nr_class = model->nr_class;
        H5::Attribute attr_nr_class = file.createAttribute("nr_class", int_type, attr_dataspace);
        attr_nr_class.write(int_type, &nr_class);
        attr_nr_class.close();
        
        //Store total_sv, number of SVs (lines), %d
        int total_sv = model->l;
        H5::Attribute attr_total_sv = file.createAttribute("total_sv", int_type, attr_dataspace);
        attr_total_sv.write(int_type, &total_sv);
        attr_total_sv.close();
        
        //everything below is stored in arrays
        int value_count = nr_class * (nr_class -1) / 2;
        
        //TODO Using chunks increases write time by alot, Chunking is not needed for the Model file so not using it
        //we create a chunk dimension for compression
        //hsize_t chunk_dims[2];
        
        //int lines_per_chunk = ((int)(total_sv/32))+1; //TODO this isn't used to just calculated on average for now
        //chunk_dims[0] = lines_per_chunk; //model->l; //this is how many rows each chunk is
        //chunk_dims[1] = model->max_idx; //this is how many columns the chunk is
        
        //create a property list, used for compression
        //H5::DSetCreatPropList proplist;
        //proplist.setChunk(2, chunk_dims); //2 because this is a 2D array
        //proplist.setDeflate(9); //0-9, lower = less compression, set highest compression
        
        //we need two differently sized dataspaces for 1D datasets
		hsize_t dim1d[1]; //1D dataspace
		dim1d[0] = value_count; //number of rows, it only has one column
		H5::DataSpace dataspace_1d_valuecount(1, dim1d); //this is 1 because this will be stored as a 1D array
        
		dim1d[0] = nr_class; //number of rows, it only has one column
		H5::DataSpace dataspace_1d_nrclass(1, dim1d); //this is 1 because this will be stored as a 1D array
        
        //Store rho values, %g
		H5::DataSet rho_class = file.createDataSet("rho", double_type, dataspace_1d_valuecount); //create the dataset, this isn't so big so no need to compress it
		rho_class.write(model->rho, double_type); //write the data to the dataset
		rho_class.close(); //close this dataset
        
        //Store label values, $d
        if(model->label)
        {
            H5::DataSet label_class = file.createDataSet("label", int_type, dataspace_1d_nrclass); //create the dataset, this isn't so big so no need to compress it
		    label_class.write(model->label, int_type); //write the data to the dataset
		    label_class.close(); //close this dataset
        }
        
        //Store probA, %g
        if(model->probA) //regression has probA only
        {
            H5::DataSet probA_class = file.createDataSet("probA", double_type, dataspace_1d_valuecount); //create the dataset, this isn't so big so no need to compress it
		    probA_class.write(model->probA, double_type); //write the data to the dataset
		    probA_class.close(); //close this dataset
        }
        
        //Store probB, %g
        if(model->probB)
        {
            H5::DataSet probB_class = file.createDataSet("probB", double_type, dataspace_1d_valuecount); //create the dataset, this isn't so big so no need to compress it
		    probB_class.write(model->probB, double_type); //write the data to the dataset
		    probB_class.close(); //close this dataset
        }
        
        //Store number of SVs, %d
        if(model->nSV)
        {
            H5::DataSet nSV_class = file.createDataSet("nr_sv", int_type, dataspace_1d_nrclass); //create the dataset, this isn't so big so no need to compress it
		    nSV_class.write(model->nSV, int_type); //write the data to the dataset
		    nSV_class.close(); //close this dataset
        }
        
        //Store the SV coefficients, create a dataspace for them
        hsize_t dim2d_coef[2]; //2D dataspace
        dim2d_coef[0] = total_sv; //number of rows
        dim2d_coef[1] = nr_class - 1; //number of columns
        H5::DataSpace dataspace_2d_coef(2, dim2d_coef); //2 because we want to save a 1D array as 2D
        
       
        //Convert the coefficients into a linear array
        double *coefficient_dataset = Calloc(double, total_sv * (nr_class -1));
        
        for (int i = 0; i < total_sv; i++)
        {
            for (int j = 0; j < nr_class -1; j++)
            {
                coefficient_dataset[ i * (nr_class -1) + j] = model->sv_coef[j][i];
            }
        }
        
        //Create and process the coefficient dataset
		H5::PredType coef_type(H5::PredType::NATIVE_DOUBLE); //define the datatype for class_dataset, H5T_NATIVE_DOUBLE
		H5::DataSet dsclass = file.createDataSet("sv_coef", coef_type, dataspace_2d_coef); //create the dataset, this isn't so big so no need to compress it
		dsclass.write(coefficient_dataset, coef_type); //write the data to the dataset
		dsclass.close(); //close this dataset
        
        free(coefficient_dataset); //Done writing so we clean up immediately
        
        //Store the SVs themselves along with a binary map to indicate if a feature:value set is part of the SV
        hsize_t dim2d_sv[2]; //2D dataspace
        dim2d_sv[0] = total_sv; //number of rows
        dim2d_sv[1] = model->max_idx; //number of columns
        H5::DataSpace dataspace_2d_sv(2, dim2d_sv); //2 because we want to save a 1D array as 2D
                
        double *sv_dataset = Calloc(double, total_sv * model->max_idx); //An array to hold the feature values
	    bool *densitymap_dataset = Calloc(bool, total_sv * model->max_idx); //An array to hold bool values that tell us if that value was in the file or not, used to create sparse data
        
        int featurecount = 0;
        for(int i = 0; i < total_sv; i++)
        {
            for(int j = 0; j < model->sv_len[i]; j++)
            {
                sv_dataset[i * model->max_idx + model->nz_sv[i][j]] = model->SV[i][j];
                densitymap_dataset[i * model->max_idx + model->nz_sv[i][j]] = true;
                featurecount++;
            }
        }
        
        //Save FeatureCount as an attribute, %d
        H5::Attribute attr_featurecount = file.createAttribute("FeatureCount", int_type, attr_dataspace);
        attr_featurecount.write(int_type, &featurecount);
        attr_featurecount.close();
        
        //process the sv_dataset dataset
		H5::PredType sv_type(H5::PredType::NATIVE_DOUBLE); //define the datatype for sv_dataset, H5T_NATIVE_FLOAT
		H5::DataSet dsSVset = file.createDataSet("SV", sv_type, dataspace_2d_sv); //, proplist);
		dsSVset.write(sv_dataset, sv_type); //write the data to the dataset
		dsSVset.close(); //close this dataset
        
		//process the densitymap dataset
		H5::PredType densitymap_type(H5::PredType::NATIVE_UCHAR); //define the datatype for densitymap_dataset, the data is bool but we store it as unsigned char for reduced space
		H5::DataSet dsdensitymap = file.createDataSet(DENSITYMAP_NAME, densitymap_type, dataspace_2d_sv); //, proplist);
		dsdensitymap.write(densitymap_dataset, densitymap_type); //write the data to the dataset
		dsdensitymap.close(); //close this dataset
	
		file.close(); //finally close the file
        
        return 0;
    }
    //Generic HDF5 exception block since we just print the error
    catch(H5::Exception ex) 
    {
        std::cerr << ex.getDetailMsg() << std::endl;
        //std::cerr << ex.getMajorString() << std::endl;
        //fprintf(stdout, "Exception happened\n");
        ex.printError(stdout);
        ex.printError(stderr);
    }
    return -1;
}
