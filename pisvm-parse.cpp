#include <string.h>
#include <time.h>
#include <iostream>
#include <exception>
#include <chrono>
#include <stdlib.h>
#include <H5Cpp.h>

//defines
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define Calloc(type,n) (type *)calloc(n, sizeof(type))
#define MAXCHAR 4096

//constants
const H5std_string	CLASSSET_NAME("class"); //double
const H5std_string	FEATURESET_NAME("feature_value"); //float
const H5std_string	DENSITYMAP_NAME("densitymap"); //bool

int chunkcount = 64; //default value for chunk count
int compressionlevel = 9; //default value for compression level

//Function that prints the help text if parameters are not correctly listed and structured
void exit_with_help()
{
    fprintf(stdout, 
        "Usage: pisvm-parse [options] input_file [output_file] \n"
		"If omitted the output filename will be the same as the source file but with an .h5 ending instead of .el ending.\n"
        "options:\n"
            "-c chunkcount : set the cluster count to use when writing data to HDF5 format (default 64)\n"
            "-z compressionlevel : set the compression level of the HDF5 file, 0-9, lower is faster but less compression (default 6)\n"
    );
    exit(1);
}


//Function to parse command line options, set variables, input and output names and validate
//that all required options are present or print out help.
void parse_command_line(int argc, char **argv, char *input_filename, char *output_filename)
{
	// parse options
	int i;
	for(i = 1; i < argc; i++)
	{
		if(argv[i][0] != '-') break; //all options must be prefixed with dash(-), no dash means no more options so we break and parse file names
		
        //can't use switch because of it only works with single characters
        if(strcmp(argv[i], "-c") == 0) //chunk count is being set
        {
            i++; //advance one position
            chunkcount = atoi(argv[i]); //and then read the value
        }
        else if (strcmp(argv[i], "-z") == 0) //compression level is being set
        {
            i++; //advance one position
            int comprlvl = atoi(argv[i]);  //read the value
            if (comprlvl < 0 || comprlvl > 9) //validate the input
                exit_with_help();
            
            compressionlevel = comprlvl;
        }
        else //no valid option
        {
            fprintf(stderr,"unknown option\n");
            exit_with_help();
        }
	}
	
	//for loop has now parsed all the options, next up we determine the file names	
	if(i>=argc) //if there are no arguments after the options (we need atleast the input file name
		exit_with_help();
	
    //if we get this far then the file name is next
    strcpy(input_filename, argv[i]);

    //Check if output filename was provided
    if(i == argc - 2) //There is exactly 1 additional parameter after the input file name
    {
        strcpy(output_filename, argv[i+1]);
    }
    else if (i == argc - 1)//input file name is the last parameter, have to construct the output
    {
        char *p = strrchr(argv[i],'/'); //there might be an absolute path provided, strip that out so that output file will go to users home directory
        if(p==NULL)
            p = argv[i];
        else
            p++;
        
        //just the filename should be left now in *p, find the last dot and remove the .el ending if it is there
        char *lastdot = strrchr(p, '.');
        if(lastdot != NULL)
            *lastdot = '\0';
        
        sprintf(output_filename,"%s.h5",p); //append an .h5 ending
    }
    else
    { //There are more parameters
        printf("ERROR: There are unparsed parameters!\n");
        exit_with_help();
    }
}


//Main function parses svmlight file into HDF5 format
int main (int argc, char **argv)
{	
    //we start recoding the time when execution starts
	std::chrono::high_resolution_clock::time_point execution_begin = std::chrono::high_resolution_clock::now();
		
	//variables to hold the input and output filenames
	char input_filename[1024];
	char output_filename[1024];
    
	//parse the command line options
	parse_command_line(argc, argv, input_filename, output_filename);
    
    fprintf(stdout, "Input file name: %s\n", input_filename);
    fprintf(stdout, "Output file name: %s\n", output_filename);
	
	//we read the source file and extract some information from it
	FILE *fp = fopen(input_filename, "r");
		
	if(fp == NULL)
    {
		fprintf(stderr, "Cannot open input file %s\n", input_filename);
		exit(1);
    }
	
	int max_feature_id = 0; //we will need this many columns in the hdf5 array
	int line_count = 0; //we will need this many rows in the hdf5 array
	
	//first we loop through the whole file and find the max_feature_id and line_count
	char line[MAXCHAR];
	while(fgets(line, MAXCHAR, fp) != NULL)
	{
		//remove the newline character from the end of the string, or it would be counted as a token by strtok
		//strtok stops reading at '\0' so we just swap out the newline char for that
		char *nulltoken;
		if((nulltoken = strchr(line, '\n')) != NULL)
		{
			*nulltoken = '\0';
		}
		
		//we tokenize the string array first based on spaces
		char *token;
		token = strtok(line, " "); //we get the first token, that is the label, do nothing with it for now
		token = strtok(NULL, " "); //we then get the next token, those should all be sets of feature:value pairs until the end of the line
		
		while (token)
		{
			//Now we need to split the token based on ":"
			int featureid;
			float featurevalue;
			
			sscanf(token, "%d:%f", &featureid, &featurevalue);
						
			if (max_feature_id < featureid) //if we find a max_feature_id higher than the ones already found we store it
				max_feature_id = featureid;
						
			//get the next token, if no token exists then NULL is returned
			token = strtok(NULL, " ");
		}
		
		line_count++; //increment the line count
	}
	
	//now we rewind the stream, loop through it again, and parse all the content into the value arrays
	rewind(fp);
	
	//These arrays must be allocated on the heap because of the size, we use Calloc so that all values are initialized to 0
	double *class_dataset = Calloc(double, line_count); //An array to hold the class labels
	float *feature_dataset = Calloc(float, line_count * max_feature_id); //An array to hold the feature values
	bool *densitymap_dataset = Calloc(bool, line_count * max_feature_id); //An array to hold bool values that tell us if that value was in the file or not, used to create sparse data
	
	//we now loop through the file again and put all values into their respective arrays	
	int linepos = 0;
	int sparsefeaturecount = 0; //we will figure this out as we read the data
	int densefeaturecount = line_count * max_feature_id; //we will store this aswell for faster access
	
	while(fgets(line, MAXCHAR, fp) != NULL)
	{
		//remove the newline character from the end of the string, or it would be counted as a token by strtok
		//strtok stops at '\0' so we just swap out the newline char
		char *nulltoken;
		if((nulltoken = strchr(line, '\n')) != NULL)
		{
			*nulltoken = '\0';
		}
		
		//we tokenize the string array first based on spaces
		char *token;
		token = strtok(line, " "); //we get the first token, that is the label, 
		class_dataset[linepos] = atof(token); //and store it in its own dataset

		token = strtok(NULL, " "); //we then get the next token, those should all be sets of feature:value pairs until the end of the line
		
		while (token)
		{
			//Now we need to split the token based on ":"
			int featureid;
			float featurevalue;
			sscanf(token, "%d:%f", &featureid, &featurevalue);
			
			//We save them in their respective array
			//Feature:Value will always have the first Feature ID as 1 but we are working with a 0-based DataSet
			feature_dataset[linepos * max_feature_id + (featureid - 1)] = featurevalue;
			densitymap_dataset[linepos * max_feature_id + (featureid - 1)] = true;
			sparsefeaturecount++;
						
			//get the next token, if no token exists this is null
			token = strtok(NULL, " ");
		}
		
		linepos++;
	}
	
	//close the stream
	fclose(fp);
	
	//and now we can create the hdf5 file, put the arrays into datasets and save it
	try
    {
		//Turn off the auto-printing when failure occurs so that we can handle the errors appropriately
		H5::Exception::dontPrint();
		
		//we must create two different types of dataspaces
		hsize_t dim1d[1]; //1D dataspace
		dim1d[0] = line_count; //rows, it only has one column
		H5::DataSpace dataspace_1d(1, dim1d); //this is 1 because this will be stored as a 1D array
		
		hsize_t dim2d[2];
		dim2d[0] = line_count;
		dim2d[1] = max_feature_id;
		H5::DataSpace dataspace_2d(2, dim2d); //2 because we want to save a 1D array as 2D
        
        //we create a chunk dimension for compression
        hsize_t chunk_dims[2];
        
        //we need to figure out how many lines per chunk, we use the line count and chunk count
        //optimally this should be divisible by the number of cores, default is 64, if 64 cores are used then each gets one chunk, 32 cores each one gets two chunks etc
        int lines_per_chunk = ((int)(line_count/chunkcount))+1; //we find the remainder of the linecount divided by 128 (that will be the max number of cpus)
        fprintf(stdout, "Lines per chunk: %i\n", lines_per_chunk);

        chunk_dims[0] = lines_per_chunk; //this is how many rows each chunk is
        chunk_dims[1] = max_feature_id; //this is how many columns the chunk is
        
        //create a property list, used for compression
        H5::DSetCreatPropList proplist;
        proplist.setChunk(2, chunk_dims); //2 because this is a 2D array
        proplist.setDeflate(compressionlevel); //0-9, lower means less compression and faster access times

		//create the file
		H5::H5File file(output_filename, H5F_ACC_TRUNC);

		//process the class dataset
		H5::PredType class_type(H5::PredType::NATIVE_DOUBLE); //define the datatype for class_dataset, H5T_NATIVE_DOUBLE
		class_type.setOrder(H5T_ORDER_LE); //set order to little endian
		H5::DataSet dsclass = file.createDataSet(CLASSSET_NAME, class_type, dataspace_1d); //create the dataset, this isn't so big so no need to compress it
		dsclass.write(class_dataset, class_type); //write the data to the dataset
		dsclass.close(); //close this dataset
		
		//process the feature_value dataset
		H5::PredType feature_type(H5::PredType::NATIVE_FLOAT); //define the datatype for feature_dataset, H5T_NATIVE_FLOAT
		feature_type.setOrder(H5T_ORDER_LE); //set order to little endian
		H5::DataSet dsfeatureset = file.createDataSet(FEATURESET_NAME, feature_type, dataspace_2d, proplist);
		dsfeatureset.write(feature_dataset, feature_type); //write the data to the dataset
		dsfeatureset.close(); //close this dataset
		
		//process the densitymap dataset
		H5::PredType densitymap_type(H5::PredType::NATIVE_UCHAR); //define the datatype for densitymap_dataset, the data is bool but we store it as unsigned char for reduced space
		densitymap_type.setOrder(H5T_ORDER_LE); //set order to little endian
		H5::DataSet dsdensitymap = file.createDataSet(DENSITYMAP_NAME, densitymap_type, dataspace_2d, proplist);
		dsdensitymap.write(densitymap_dataset, densitymap_type); //write the data to the dataset
		dsdensitymap.close(); //close this dataset
		
		//we can now write attributes, they are all INTs so we just use that one type
		H5::PredType attr_type(H5::PredType::NATIVE_INT);
		H5::DataSpace attr_space(H5S_SCALAR);
		
        //Store the SparseFeatureCount so we can reconstruct the data into arrays
		H5::Attribute attr_sparsefeaturecount = file.createAttribute("SparseFeatureCount", attr_type, attr_space);
		attr_sparsefeaturecount.write(attr_type, &sparsefeaturecount);
		attr_sparsefeaturecount.close();
		
        //Store the DenseFeatureCount although it is just line_count * max_feature_id
		H5::Attribute attr_densefeaturecount = file.createAttribute("DenseFeatureCount", attr_type, attr_space);
		attr_densefeaturecount.write(attr_type, &densefeaturecount);
		attr_densefeaturecount.close();
        
        //Store ChunkCount, programs that read the HDF5 file can use that to optimize their read pattern
        H5::Attribute attr_chunkcount = file.createAttribute("ChunkCount", attr_type, attr_space);
		attr_chunkcount.write(attr_type, &chunkcount);
		attr_chunkcount.close();
        
        //Store the compression level, it is not needed for anything but good information for someone using the file
        H5::Attribute attr_compressionlevel = file.createAttribute("CompressionLevel", attr_type, attr_space);
		attr_compressionlevel.write(attr_type, &compressionlevel);
		attr_compressionlevel.close();
	
		file.close(); //finally close the file
    }  // end of try block
    catch(H5::Exception ex ) //Just one generic catch block
	{
		ex.printError(); //print the error
        
        //cleanup
        free(class_dataset);
	    free(feature_dataset);
	    free(densitymap_dataset);
        return -1; //return with a fail
	}
    
	//Cleanup
	free(class_dataset);
	free(feature_dataset);
	free(densitymap_dataset);
	
	//record the stop time and print it
    std::chrono::high_resolution_clock::time_point execution_end = std::chrono::high_resolution_clock::now();
    int executiontime = std::chrono::duration_cast<std::chrono::milliseconds>(execution_end - execution_begin).count();
	
	fprintf(stdout, "Parsing took: %d milliseconds\n", executiontime);
    fprintf(stdout, "File parameters: chunk size %i, compression level %i\n", chunkcount, compressionlevel);
    
    return 0;  // successfully terminated
}