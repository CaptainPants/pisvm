#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <chrono>
#include <string.h>
#include <mpi.h>
#include "svm.h"
#include <iostream>
#include <time.h>
#include <H5Cpp.h>

//defines
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define Calloc(type,n) (type *)calloc(n, sizeof(type))

//constants
const H5std_string    CLASSSET_NAME("class"); //double
const H5std_string    FEATURESET_NAME("feature_value"); //float
const H5std_string    DENSITYMAP_NAME("densitymap"); //bool

char* line;
int max_line_len = 1024;
Xfloat *x;
int *nz_x;
int max_nr_attr = 64;

struct svm_model* model;
int predict_probability=0;

// Determines the number of input patterns.
int num_patterns(FILE *input)
{
    int l=0;
    char c;
    do {
        c = getc(input);
        if(c=='\n')
            ++l;
    } while(c!=EOF);
    rewind(input);
    return l;
}

void setup_range(int *range_low, int *range_up, int total_sz, int size)
{
    int local_sz = total_sz/size;
    int idx_up = local_sz;
    int idx_low = 0;
    if(total_sz != 0)
    {
        for(int i=0; i<size-1; ++i)
        {
            range_low[i] = idx_low;
            range_up[i] = idx_up;
            idx_low = idx_up;
            idx_up = idx_low + local_sz + 1;
        }
        range_low[size-1] = idx_low;
        range_up[size-1]=total_sz;
    }
    else
    {
        for(int i=0; i<size; ++i)
        {
            range_low[i] = 0;
            range_up[i] = 0;
        }
    }
}


void setup_range_hdf5(int *range_low, int *range_up, int total_linecount, int no_of_processors, int chunks_in_file)
{
    if (total_linecount <= chunks_in_file) //there is not one line per chunk, so we don't worry about the chunks and just divide the lines evenly between processors
    {
        setup_range(range_low, range_up, total_linecount, no_of_processors);
    }
    else
    {
        //Store each ranks lower and upper line count in range_low and range_up
        int lines_per_chunk = ((int)(total_linecount/chunks_in_file))+1; //Calculate how many lines are in each chunk based on total_linecount and chunks_in_file
        int chunks_per_cpu = chunks_in_file/no_of_processors;
        
        int lines_per_cpu = (total_linecount/no_of_processors); //used if each processor cannot get a minimum of one chunk to read

        //If each processor can get a minimum of one chunk
        if (chunks_per_cpu > no_of_processors)
        {
            lines_per_cpu = lines_per_chunk * chunks_per_cpu;
        }
        
        int line_low = 0;
        int line_up = lines_per_cpu;

        if(total_linecount != 0)
        {
            for(int i = 0; i < no_of_processors-1; i++)
            {
                range_low[i] = line_low;
                range_up[i] = line_up;

                line_low = line_up;
                line_up = line_low + lines_per_cpu;
            }
            range_low[no_of_processors-1] = line_low;
            range_up[no_of_processors-1]=total_linecount;
        }
        else
        {
            for(int i=0; i < no_of_processors; ++i)
            {
                range_low[i] = 0;
                range_up[i] = 0;
            }
        }
    }
}


void predict_parallel(FILE *input, FILE *output, int l, MPI_Comm comm)
{
    int rank, size;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    
    // Determine local range
    int l_low_loc, l_up_loc;
    int local_l = 0;
    int correct = 0;
    int other_correct;
    int total = 0;
    int ierr = 0;
    double error = 0;
    double other_error;
    double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
    double other_sumv = 0, other_sumy = 0, other_sumvv = 0, other_sumyy = 0;
    double other_sumvy = 0;
    int svm_type=svm_get_svm_type(model);
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    double *v;
    double target;
    int j,k,jj;
    int *l_up = (int *) malloc(size*sizeof(int));
    int *l_low = (int *) malloc(size*sizeof(int));

    setup_range(l_low, l_up, l, size);
    l_low_loc = l_low[rank];
    l_up_loc = l_up[rank];
    local_l = l_up_loc - l_low_loc;
    v = (double *) malloc((local_l+(size-1))*sizeof(double));

    if(predict_probability)
    {
        if(!(svm_type==NU_SVR || svm_type==EPSILON_SVR))
        {
            prob_estimates = (double *) malloc(((local_l+(size-1))*nr_class)*
                                               sizeof(double));
        }
    }

    //Start recording the time when Input reading starts
    std::chrono::high_resolution_clock::time_point input_begin = std::chrono::high_resolution_clock::now();
    
    k=0;
    while(1)
    {
        int i = 0;
        int c;

        if (fscanf(input,"%lf",&target)==EOF)
            break;
        while(1) //TODO: skip lines in file this process is not responsible for (l_low_loc <= total < l_up_loc)
        {
            if(i>=max_nr_attr-1)	// need one more for index = -1
            {
                max_nr_attr *= 2;
                x = (Xfloat *) realloc(x,max_nr_attr*sizeof(double));
                nz_x = (int *) realloc(nz_x,max_nr_attr*sizeof(int));
            }

            do {
                c = getc(input);
                if(c=='\n' || c==EOF) goto out2;
            } while(isspace(c));
            ungetc(c,input);
            //	  fscanf(input,"%d:%lf",&nz_x[i],&x[i]);
            fscanf(input,"%d:%f",&nz_x[i],&x[i]);
            --nz_x[i]; // we need zero based indices
            ++i;
        }
out2:
        if(l_low_loc <= total && total < l_up_loc)
        {
            if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
            {
                v[k] = svm_predict_probability(model, x, nz_x, i, &prob_estimates[k*nr_class]);
            }
            else
            {
                v[k] = svm_predict(model, x, nz_x, i);
            }
            if(v[k] == target)
                ++correct;
            error += (v[k]-target)*(v[k]-target);
            sumv += v[k];
            sumy += target;
            sumvv += v[k]*v[k];
            sumyy += target*target;
            sumvy += v[k]*target;
            ++k;
        }
        ++total;
    }
    
    //Here IO is done
    std::chrono::high_resolution_clock::time_point input_end = std::chrono::high_resolution_clock::now();
    
    //Create an instance of output measuring variable
    std::chrono::high_resolution_clock::time_point output_begin;
    std::chrono::high_resolution_clock::time_point output_end;
    
    // Send all predictions to first processor, first processor
    // writes output file
    if(rank == 0)
    {
        //Start recording the Output time only for Rank 0
        output_begin = std::chrono::high_resolution_clock::now();
        
        if(predict_probability)
        {
            if (svm_type==NU_SVR || svm_type==EPSILON_SVR)
            {
                printf("Prob. model for test data: target value = predicted");
                printf("value + z,\nz: Laplace distribution e^(-|z|/sigma)/");
                printf("2sigma),sigma=%g\n",svm_get_svr_probability(model));
            }
            else
            {
                int *labels=(int *) malloc(nr_class*sizeof(int));
                svm_get_labels(model,labels);
                fprintf(output,"labels");
                for(j=0; j<nr_class; j++)
                    fprintf(output," %d",labels[j]);
                fprintf(output,"\n");
                free(labels);
            }
        }
        for(k=0; k<local_l; ++k)
        {
            if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
            {
                fprintf(output,"%g ",v[k]);
                for(j=0; j<nr_class; j++)
                    fprintf(output,"%g ",prob_estimates[k*nr_class+j]);
                fprintf(output,"\n");
            }
            else
            {
                fprintf(output,"%g\n",v[k]);
            }
        }
        for(j=1; j<size; ++j) 
        {
            MPI_Status stat;
            //TODO: MPI_Reduce?
            ierr = MPI_Recv(v, l_up[j]-l_low[j], MPI_DOUBLE,
                            j, 0, comm, &stat);
            ierr = MPI_Recv(&other_correct, 1, MPI_INT, j, 0, comm, &stat);
            correct += other_correct;
            ierr = MPI_Recv(&other_sumv, 1, MPI_DOUBLE, j, 0, comm, &stat);
            sumv += other_sumv;
            ierr = MPI_Recv(&other_sumy, 1, MPI_DOUBLE, j, 0, comm, &stat);
            sumy += other_sumy;
            ierr = MPI_Recv(&other_sumvy, 1, MPI_DOUBLE, j, 0, comm, &stat);
            sumvy += other_sumvy;
            ierr = MPI_Recv(&other_sumvv, 1, MPI_DOUBLE, j, 0, comm, &stat);
            sumvv += other_sumvv;
            ierr = MPI_Recv(&other_sumyy, 1, MPI_DOUBLE, j, 0, comm, &stat);
            sumyy += other_sumyy;
            ierr = MPI_Recv(&other_error, 1, MPI_DOUBLE, j, 0, comm, &stat);
            error += other_error;
            if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
            {
                ierr = MPI_Recv(prob_estimates, (l_up[j]-l_low[j])*nr_class,
                                MPI_DOUBLE, j, 0, comm, &stat);
                for(k=0; k<l_up[j]-l_low[j]; ++k)
                {
                    fprintf(output,"%g ",v[k]);
                    for(jj=0; jj<nr_class; jj++)
                        fprintf(output,"%g ",prob_estimates[k*nr_class+jj]);
                    fprintf(output,"\n");
                }
            }
            else
            {
                for(k=0; k<l_up[j]-l_low[j]; ++k)
                {
                    fprintf(output,"%g\n",v[k]);
                }
            }
        }
        printf("Accuracy = %g%% (%d/%d) (classification)\n",
               (double)correct/total*100,correct,total);
        printf("Mean squared error = %g (regression)\n",error/total);
        printf("Squared correlation coefficient = %g (regression)\n",
               ((total*sumvy-sumv*sumy)*(total*sumvy-sumv*sumy))/
               ((total*sumvv-sumv*sumv)*(total*sumyy-sumy*sumy))
              );
        
        output_end = std::chrono::high_resolution_clock::now();
    } // if(rank == 0)
    else
    {
        //TODO: MPI_Reduce?
        ierr = MPI_Send(v, local_l, MPI_DOUBLE, 0, 0, comm);
        ierr = MPI_Send(&correct, 1, MPI_INT, 0, 0, comm);
        ierr = MPI_Send(&sumv, 1, MPI_DOUBLE, 0, 0, comm);
        ierr = MPI_Send(&sumy, 1, MPI_DOUBLE, 0, 0, comm);
        ierr = MPI_Send(&sumvy, 1, MPI_DOUBLE, 0, 0, comm);
        ierr = MPI_Send(&sumvv, 1, MPI_DOUBLE, 0, 0, comm);
        ierr = MPI_Send(&sumyy, 1, MPI_DOUBLE, 0, 0, comm);
        ierr = MPI_Send(&error, 1, MPI_DOUBLE, 0, 0, comm);

        if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
        {
            ierr = MPI_Send(prob_estimates, local_l*nr_class,
                            MPI_DOUBLE, 0, 0, comm);
        }
    }
    
    //Print out the input/processing time
    int inputtime = std::chrono::duration_cast<std::chrono::milliseconds>(input_end - input_begin).count();
    printf("Rank %d - Input and processing time: %d\n", rank, inputtime);

    if (rank == 0)
    {
        //Print out the Output time
        int outputtime = std::chrono::duration_cast<std::chrono::milliseconds>(output_end - output_begin).count();
        printf("Rank %d - Output time: %d\n", rank, outputtime);
    }

    if(predict_probability)
    {
        free(prob_estimates);
    }
    free(v);
    free(l_up);
    free(l_low);
}


void predict_parallel_hdf5(const char *input_filename, FILE *output, MPI_Comm comm)
{
    int rank, size;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    
    // Determine local range
    int l_low_loc, l_up_loc;
    int local_l = 0;
    int correct = 0; //this stores the correct values that the processor found
    int other_correct; //this stores the other correct values when doing mpi_send and receive
    int ierr = 0;
    double error = 0;
    double other_error;
    double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
    double other_sumv = 0, other_sumy = 0, other_sumvv = 0, other_sumyy = 0, other_sumvy = 0;
    int svm_type=svm_get_svm_type(model); //model is global
    int nr_class=svm_get_nr_class(model);
    double *prob_estimates=NULL;
    double *v;
    double target;
    int j, k, jj;
    int *l_up = (int *) malloc(size*sizeof(int));
    int *l_low = (int *) malloc(size*sizeof(int));

    //Start recording the input time
    std::chrono::high_resolution_clock::time_point input_begin = std::chrono::high_resolution_clock::now();
        
    //we have to read the hdf5 file and get the line count before setting range
    try
    {
        H5::Exception::dontPrint();
        H5::H5File file(input_filename, H5F_ACC_RDONLY ); //read access only
        
        //we open the three datasets from the file
        H5::DataSet dsclass = file.openDataSet( CLASSSET_NAME );
        H5::DataSet dsfeatureset = file.openDataSet( FEATURESET_NAME );
        H5::DataSet dsdensitymap = file.openDataSet( DENSITYMAP_NAME );
                
        //Data can be sparse so we have to read the SparseFeatureCount, if the data is naturally dense it wont matter
        int featurecount = 0;
        H5::Attribute attr_featurecount = file.openAttribute("SparseFeatureCount");
        H5::DataType sfc_type = attr_featurecount.getDataType(); //Extract the datatype
        attr_featurecount.read(sfc_type, &featurecount); //Read the feature count value
        
        //The chunk count is written in the file metadata
        int chunkcount = 0;
        H5::Attribute attr_chunkcount = file.openAttribute("ChunkCount");
        H5::DataType cc_type = attr_chunkcount.getDataType(); //Extract the datatype
        attr_chunkcount.read(cc_type, &chunkcount); //Read the feature count value
        
        //Two dataspaces are required, the dataspace for dsfeatureset and dsdensitymap is the same
        H5::DataSpace filespace1d = dsclass.getSpace();
        H5::DataSpace filespace2d = dsfeatureset.getSpace();

        //extract the amount of records(lines) and features(columns) in dsfeatureset
        hsize_t dims_out[2];
        int ndims = filespace2d.getSimpleExtentDims(dims_out, NULL);
        int line_count = (int)dims_out[0];
        int max_feature_id = (int)dims_out[1];
        
        //We reuse this method but calculate the lines based on chunking in a different way, l_low and l_up are used in mpi_send and receive
        setup_range_hdf5(l_low, l_up, line_count, size, chunkcount); //assigns upper and lower line counts based on the number of processors
        l_low_loc = l_low[rank]; //local lower line count (specific for this processors)
        l_up_loc = l_up[rank]; //local upper line count (specific for this processor)
        local_l = l_up_loc - l_low_loc; //local total line count
        v = (double *) malloc((local_l+(size-1))*sizeof(double)); //assign v enough space for those lines
        
        //how we can use the local lower and local upper to set the count and offset for this processors particular read section
        hsize_t offset[2] = { l_low_loc, 0 }; //{ start_at_line, start_at_column }, start_at_line is relevanto the processor rank
        hsize_t count_1d[2] = { local_l, 1 }; //this is for reading the class_dataset since it is 1d
        hsize_t count_2d[2] = { local_l, max_feature_id  }; //lines read is relevant to the processor rank.
        
        //These arrays must be allocated on the heap because of the size, they will contain the data from the hdf5 file
        double *class_dataset = Malloc(double, local_l); //Array to hold the class values
        float *feature_dataset = Malloc(float, local_l * max_feature_id); //Array to hold the feature values
        bool *densitymap_dataset = Malloc(bool, local_l * max_feature_id); //Array with bool values, true if the value was found in the file, used to create sparse and dense data
        
        //Set up the hyperslab
        filespace1d.selectHyperslab(H5S_SELECT_SET, count_1d, offset);
        filespace2d.selectHyperslab(H5S_SELECT_SET, count_2d, offset);

        //we need two memory dataspaces, one for dim1 and one for dim2
        hsize_t col_dims1[1];
        col_dims1[0] = local_l;
        H5::DataSpace mspace_1d(1, col_dims1);

        hsize_t col_dims2[2];
        col_dims2[0] = local_l;
        col_dims2[1] = max_feature_id;
        H5::DataSpace mspace_2d(2, col_dims2);

        //we now read the dataset
        dsclass.read(class_dataset, H5::PredType::NATIVE_DOUBLE, mspace_1d, filespace1d);
        dsfeatureset.read(feature_dataset, H5::PredType::NATIVE_FLOAT, mspace_2d, filespace2d);
        dsdensitymap.read(densitymap_dataset, H5::PredType::NATIVE_UCHAR, mspace_2d, filespace2d);

        //Input read is done
        std::chrono::high_resolution_clock::time_point input_end = std::chrono::high_resolution_clock::now();
        
        //Start recording processing time
        std::chrono::high_resolution_clock::time_point processing_begin = std::chrono::high_resolution_clock::now();

        if(predict_probability)
        {
            if(!(svm_type==NU_SVR || svm_type==EPSILON_SVR))
            {
                prob_estimates = (double *) malloc(((local_l+(size-1))*nr_class)*sizeof(double));
            }
        }

        k = 0;
        for (int i = 0; i < local_l; i++) //this process only reads the lines that belong to him, they should all be chunked
        {
            //we have to figure out how many features were in this particular line
            //we do that by examining how often true is recorded in densitymap_dataset
            int line_featurecount = 0;
            for (int e = 0; e < max_feature_id; e++)
            {
                if (densitymap_dataset[i * max_feature_id + e] == 1) //if the value was in the dataset
                {
                    line_featurecount++;
                }
            }

            //we allocate space based on the feature count in this particular line
            x = Malloc(Xfloat, line_featurecount);
            nz_x = Malloc(int, line_featurecount);

            target = class_dataset[i]; //we can just straight up store the target
            int j = 0; //we use j to keep track of the location in the sparse datasets
            for (int e = 0; e < max_feature_id; e++)
            {
                if(densitymap_dataset[i * max_feature_id + e] == 1)
                {
                    //create the two datasets of indexes and values that will be used for prediction
                    nz_x[j] = e; //the IDs are 0-based in HDF5 so we can just straight up store them
                    x[j] = feature_dataset[i * max_feature_id + e];
                    j++; //value was found, increment j
                }
            }
           
            if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
            {
                v[k] = svm_predict_probability(model, x, nz_x, line_featurecount, &prob_estimates[k*nr_class]);
            }
            else
            {
                v[k] = svm_predict(model, x, nz_x, line_featurecount);
            }
            if(v[k] == target)
                correct++;
            error += (v[k]-target)*(v[k]-target);
            sumv += v[k];
            sumy += target;
            sumvv += v[k]*v[k];
            sumyy += target*target;
            sumvy += v[k]*target;
            k++;
        }
        
        //Processing is done
        std::chrono::high_resolution_clock::time_point processing_end = std::chrono::high_resolution_clock::now();
        
        //Create an instance of output measuring variable
        std::chrono::high_resolution_clock::time_point output_begin;
        std::chrono::high_resolution_clock::time_point output_end;
        
        //everything below this is unmodified from the original
        // Send all predictions to first processor, first processor
        // writes output file
        if(rank == 0)
        {
            //Start recording output time
            output_begin = std::chrono::high_resolution_clock::now();
            
            if(predict_probability)
            {
                if (svm_type==NU_SVR || svm_type==EPSILON_SVR)
                {
                    printf("Prob. model for test data: target value = predicted");
                    printf("value + z,\nz: Laplace distribution e^(-|z|/sigma)/");
                    printf("2sigma),sigma=%g\n",svm_get_svr_probability(model));
                }
                else
                {
                    int *labels=(int *) malloc(nr_class*sizeof(int));
                    svm_get_labels(model,labels);
                    fprintf(output,"labels");
                    for(j=0; j<nr_class; j++)
                        fprintf(output," %d",labels[j]);
                    fprintf(output,"\n");
                    free(labels);
                }
            }
            for(k=0; k < local_l; ++k)
                if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
                {
                    fprintf(output,"%g ",v[k]);
                    for(j=0; j<nr_class; j++)
                        fprintf(output,"%g ",prob_estimates[k*nr_class+j]);
                    fprintf(output,"\n");
                }
                else
                {
                    fprintf(output,"%g\n",v[k]);
                }
            for(j=1; j<size; ++j) {
                MPI_Status stat;
                //TODO: MPI_Reduce?
                ierr = MPI_Recv(v, l_up[j]-l_low[j], MPI_DOUBLE, j, 0, comm, &stat);
                ierr = MPI_Recv(&other_correct, 1, MPI_INT, j, 0, comm, &stat);
                correct += other_correct;
                ierr = MPI_Recv(&other_sumv, 1, MPI_DOUBLE, j, 0, comm, &stat);
                sumv += other_sumv;
                ierr = MPI_Recv(&other_sumy, 1, MPI_DOUBLE, j, 0, comm, &stat);
                sumy += other_sumy;
                ierr = MPI_Recv(&other_sumvy, 1, MPI_DOUBLE, j, 0, comm, &stat);
                sumvy += other_sumvy;
                ierr = MPI_Recv(&other_sumvv, 1, MPI_DOUBLE, j, 0, comm, &stat);
                sumvv += other_sumvv;
                ierr = MPI_Recv(&other_sumyy, 1, MPI_DOUBLE, j, 0, comm, &stat);
                sumyy += other_sumyy;
                ierr = MPI_Recv(&other_error, 1, MPI_DOUBLE, j, 0, comm, &stat);
                error += other_error;
                if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
                {
                    ierr = MPI_Recv(prob_estimates, (l_up[j]-l_low[j])*nr_class, MPI_DOUBLE, j, 0, comm, &stat);
                    for(k=0; k<l_up[j]-l_low[j]; ++k)
                    {
                        fprintf(output,"%g ",v[k]);
                        for(jj=0; jj<nr_class; jj++)
                            fprintf(output,"%g ",prob_estimates[k*nr_class+jj]);
                        fprintf(output,"\n");
                    }
                }
                else
                {
                    for(k=0; k<l_up[j]-l_low[j]; ++k)
                    {
                        fprintf(output,"%g\n",v[k]);
                    }
                }
            }
            printf("Accuracy = %g%% (%d/%d) (classification)\n", (double)correct/line_count*100,correct,line_count);
            printf("Mean squared error = %g (regression)\n",error/line_count);
            printf("Squared correlation coefficient = %g (regression)\n",
                   ((line_count*sumvy-sumv*sumy)*(line_count*sumvy-sumv*sumy))/
                   ((line_count*sumvv-sumv*sumv)*(line_count*sumyy-sumy*sumy))
                  );
            
            //Output is done
            output_end = std::chrono::high_resolution_clock::now();
        } // if(rank == 0)
        else
        {
            //TODO: MPI_Reduce?
            ierr = MPI_Send(v, local_l, MPI_DOUBLE, 0, 0, comm);
            ierr = MPI_Send(&correct, 1, MPI_INT, 0, 0, comm);
            ierr = MPI_Send(&sumv, 1, MPI_DOUBLE, 0, 0, comm);
            ierr = MPI_Send(&sumy, 1, MPI_DOUBLE, 0, 0, comm);
            ierr = MPI_Send(&sumvy, 1, MPI_DOUBLE, 0, 0, comm);
            ierr = MPI_Send(&sumvv, 1, MPI_DOUBLE, 0, 0, comm);
            ierr = MPI_Send(&sumyy, 1, MPI_DOUBLE, 0, 0, comm);
            ierr = MPI_Send(&error, 1, MPI_DOUBLE, 0, 0, comm);

            if (predict_probability && (svm_type==C_SVC || svm_type==NU_SVC))
            {
                ierr = MPI_Send(prob_estimates, local_l*nr_class,
                                MPI_DOUBLE, 0, 0, comm);
            }
        }

        if(predict_probability)
        {
            free(prob_estimates);
        }
        free(v);
        free(l_up);
        free(l_low);
        free(class_dataset);
        free(feature_dataset);
        free(densitymap_dataset);
        
        //Print input time
        int inputtime = std::chrono::duration_cast<std::chrono::milliseconds>(input_end - input_begin).count();
        printf("Rank: %d - Input time: %d\n", rank, inputtime);
            
        //Print processing time, only need one rank to do that
        int processingtime = std::chrono::duration_cast<std::chrono::milliseconds>(processing_end - processing_begin).count();
        printf("Rank: %d - Processing time: %d\n", rank, processingtime);
        
        //print output time, only Rank 0 writes output so only he records it
        if(rank == 0)
        {
            int outputtime = std::chrono::duration_cast<std::chrono::milliseconds>(output_end - output_begin).count();
            printf("Rank: %d - Output time: %d\n", rank, outputtime);
        }
    }
    catch(H5::Exception ex ) //Just one generic catch block
	{
		ex.printError(stderr); //print the error
    }
}


svm_model *svm_load_model_hdf5(const char *model_file_name)
{
    //Initialize the model
    svm_model *model = Malloc(svm_model, 1);
    svm_parameter &param = model->param;
    model->rho = NULL;
    model->probA = NULL;
    model->probB = NULL;
    model->label = NULL;
    model->nSV = NULL;
    model->sv_len = NULL;
    
    try
    {
        H5::Exception::dontPrint();
        H5::H5File file(model_file_name, H5F_ACC_RDONLY ); //open the file, read access only

        //First read in stuff from attributes
        //svm_type, %s
        char svm_type[20];
        H5::Attribute attr_svm_type = file.openAttribute("svm_type");
        attr_svm_type.read(attr_svm_type.getDataType(), &svm_type);
            
        //validate the svm_type
        int i = 0;
        for(i = 0; svm_type_table[i]; i++)
        {
            if(strcmp(svm_type_table[i], svm_type) == 0)
            {    
                param.svm_type = i;
                break;
            }
        }
        if(svm_type_table[i] == NULL)
        {
            fprintf(stderr,"unknown svm type.\n");
            free(model);
            return NULL;
        }
        
        //kernel_type, %s
        char kernel_type[20];
        H5::Attribute attr_kernel_type = file.openAttribute("kernel_type");
        attr_kernel_type.read(attr_kernel_type.getDataType(), &kernel_type);
       
        //validate the kernel_type
        for(i=0; kernel_type_table[i]; i++)
        {
            if(strcmp(kernel_type_table[i], kernel_type)==0)
            {
                param.kernel_type=i;
                break;
            }
        }
        if(kernel_type_table[i] == NULL)
        {
            fprintf(stderr,"unknown kernel function.\n");
            free(model);
            return NULL;
        }
        
        //degree, %d, degree only exists in the file if kernel_type == POLY
        //so we have to check if it exists as an attribute
        if (file.attrExists("degree"))
        {
            H5::Attribute attr_degree = file.openAttribute("degree");
            attr_degree.read(attr_degree.getDataType(), &param.degree);
        }
        
        //gamma, %g
        if (file.attrExists("gamma"))
        {
            H5::Attribute attr_gamma = file.openAttribute("gamma");
            attr_gamma.read(attr_gamma.getDataType(), &param.gamma);
        }
        
        //coef0, %g
        if (file.attrExists("coef0"))
        {
            H5::Attribute attr_coef0 = file.openAttribute("coef0");
            attr_coef0.read(attr_coef0.getDataType(), &param.coef0);
        }
        
        //nr_class, %d
        H5::Attribute attr_nr_class = file.openAttribute("nr_class");
        attr_nr_class.read(attr_nr_class.getDataType(), &model->nr_class);

        //total_sv, %d
        H5::Attribute attr_total_sv = file.openAttribute("total_sv");
        attr_total_sv.read(attr_total_sv.getDataType(), &model->l);
        
        //This is a common size for most of the datasets
        int value_count = model->nr_class * (model->nr_class -1) / 2; //this is used to calculate space for rho, probA and probB
        
        //Read dataset values
        //rho, %lf
        H5::DataSet dsrho = file.openDataSet( "rho" );
		H5::DataSpace dim_rho = dsrho.getSpace();
        model->rho = Malloc(double, value_count);
        dsrho.read(model->rho, H5::PredType::NATIVE_DOUBLE, dim_rho);
        
        //fprintf(stderr, "rho was read\n");
        
        //label, %d
        try //Can't ask if a dataset exists, have to try to open it and catch the error
        {
            H5::DataSet dslabel = file.openDataSet("label");
            H5::DataSpace dim_label = dslabel.getSpace();
            model->label = Malloc(int, model->nr_class);
            dslabel.read(model->label, H5::PredType::NATIVE_INT, dim_label);
        }
        catch (H5::FileIException ex) //Catch only this exception, others should propagate further
        {
            fprintf(stdout, "Dataset 'label' not found\n");
        }
        
        try
        {
            H5::DataSet dsprobA = file.openDataSet("probA");
            H5::DataSpace dim_probA = dsprobA.getSpace();
            model->probA = Malloc(double, value_count);
            dsprobA.read(model->probA, H5::PredType::NATIVE_DOUBLE, dim_probA);
        }
        catch(H5::FileIException ex)
        {
            fprintf(stdout, "Dataset 'probA' not found\n");
        }

        //probB, %lf
        try
        {
            H5::DataSet dsprobB = file.openDataSet("probB");
            H5::DataSpace dim_probB = dsprobB.getSpace();
            model->probB = Malloc(double, value_count);
            dsprobB.read(model->probB, H5::PredType::NATIVE_DOUBLE, dim_probB);
        }
        catch(H5::FileIException ex)
        {
            fprintf(stdout, "Dataset 'probB' not found\n");
        }
        
        //nr_sv, %d
        try
        {
            H5::DataSet dsnr_sv = file.openDataSet("nr_sv");
            H5::DataSpace dim_nr_sv = dsnr_sv.getSpace();
            model->nSV = Malloc(int, model->nr_class);
            dsnr_sv.read(model->nSV, H5::PredType::NATIVE_INT, dim_nr_sv);
        }
        catch(H5::FileIException ex)
        {
            fprintf(stdout, "Dataset 'nr_sv' not found\n");
        }
        
        //sv_coef, %lf
        H5::DataSet ds_sv_coef = file.openDataSet("sv_coef"); //Open the dataset
        H5::DataSpace dim2 = ds_sv_coef.getSpace(); //get dimensions of the dataset
        double *sv_coef_dataset = Malloc(double, model->l * (model->nr_class -1)); //allocate space
        ds_sv_coef.read(sv_coef_dataset, H5::PredType::NATIVE_DOUBLE, dim2); //read the dataset into memory
        
        //initialize the model arrays that will store the results
        model->sv_coef = Malloc (double *, model->nr_class -1);
        
        for (int i = 0; i < model->nr_class - 1; i++)
        {
            model->sv_coef[i] = Malloc(double, model->l);
        }
        
        //read it into model from the linear array
        for(int i = 0; i < model->l; i++)
        {
            for(int j = 0; j < model->nr_class -1; j++)
            {
                model->sv_coef[j][i] = sv_coef_dataset[ i * (model->nr_class -1) + j];
            }
        }
        
        free(sv_coef_dataset);
        
        //SV, %lf
        //Need to know the total FeatureCount, %d
        int featurecount;
        H5::Attribute attr_featurecount = file.openAttribute("FeatureCount");
        attr_featurecount.read(attr_featurecount.getDataType(), &featurecount);
        
        model->SV = Malloc(Xfloat *, model->l);
        model->nz_sv = Malloc(int *, model->l);
        model->sv_len = Calloc(int, model->l); //use Calloc to have all values initialized to 0
        
        H5::DataSet ds_SV = file.openDataSet("SV");
        H5::DataSet ds_densitymap = file.openDataSet("densitymap");
        H5::DataSpace dim2_SV = ds_SV.getSpace(); //bæði datasettin eru jafn stór þannig að við þurfum bara stærðir annars þeirra

        //extract the amount of records(lines) and features(columns) in dsfeatureset
		hsize_t dims_out[2];
		int ndims = dim2_SV.getSimpleExtentDims(dims_out, NULL);
		model->max_idx = (int)dims_out[1];
                
        //read the datasets
        double *SV_dataset = Malloc(double, model->l * model->max_idx);
        ds_SV.read(SV_dataset, H5::PredType::NATIVE_DOUBLE, dim2_SV);
        
        bool *densitymap_dataset = Malloc(bool, model->l * model->max_idx); //An array to hold bool values that tell us if the value was found in the file or if it is a filler and only used to create dense data
        ds_densitymap.read(densitymap_dataset, H5::PredType::NATIVE_UCHAR, dim2_SV);
        
        //now we need to populate model->SV with the SV featurevalues and model->nz_sv with the featureIDs
        int *nz_idx_space = Malloc(int, featurecount);
        Xfloat *x_space = Malloc(Xfloat, featurecount); //intermediate structs to store the feature index and feature values in each line
		
		int j = 0; //j will keep track of where we add items into x_space and nz_idx_space
		for (int i = 0; i < model->l; i++)
		{
			model->SV[i] = &x_space[j]; //and we store a reference to the first featureid
			model->nz_sv[i] = &nz_idx_space[j]; //we store a pointer to the first value
			
			for (int e = 0; e < model->max_idx; e++)
			{
				int isindataset = densitymap_dataset[i * model->max_idx + e];
				if ((bool)isindataset == true) //this value was in the dataset
				{
					nz_idx_space[j] = e ; //e is the feature index, svm_problem needs to have it zero-based, HDF5 has it stored zero-based
					x_space[j] = SV_dataset[i * model->max_idx + e];
					j++; //we found a feature:value, increment j
                    model->sv_len[i]++;
				}
			}
		}

        free(SV_dataset);
        free(densitymap_dataset);

        model->free_sv = 1;	//1 if svm_model is created by svm_load_model
        file.close();
        return model;
    }    
	//Just catch the basic H5::Exception
    catch(H5::Exception ex)
    {
        ex.printError(stderr); //print out the error
    }
    return NULL;
}


void exit_with_help()
{
    printf(
        "Usage: svm-predict [options] test_file model_file output_file\n"
        "options:\n"
        "-b probability_estimates : whether to predict probability estimates, 0 or 1 (default 0); one-class SVM not supported yet\n"
        "-f format : set the format type of the input file\n"
        "   0 -- File is in svmlight format\n"
        "   1 -- File is in HDF5 format as formatted by pisvm-parse\n"
    );
    exit(1);
}


int main(int argc, char **argv)
{
    std::chrono::high_resolution_clock::time_point execution_begin = std::chrono::high_resolution_clock::now();
    
    FILE *input, *output;
    int i;
    int fileformat = 0; //default is svmlight

    MPI_Init(&argc, &argv);
    
    // parse options
    for(i=1; i<argc; i++)
    {
        if(argv[i][0] != '-') break;
        ++i;
        switch(argv[i-1][1])
        {
        case 'b':
            predict_probability = atoi(argv[i]);
            break;
        case 'f':
            fileformat = atoi(argv[i]);
            break;
        default:
            fprintf(stderr,"unknown option\n");
            exit_with_help();
        }
    }
    if(i>=argc)
        exit_with_help();

    //the remainders of the parameters are the input file, model file and output file
    //only svmlight needs to open the file here, HDF5 reads the file in the predict_parallel_hdf5
    if (fileformat == 0)
    {
        input = fopen(argv[i],"r");
        if(input == NULL)
        {
            fprintf(stderr,"can't open input file %s\n",argv[i]);
            exit(1);
        }
    }
    
    //both formats need the model file
    if(fileformat == 0)
    {
        if((model=svm_load_model(argv[i+1]))==0)
        {
            fprintf(stderr,"can't open model file %s\n",argv[i+1]);
            exit(1);
        }
    }
    else if (fileformat == 1)
    {
        if((model=svm_load_model_hdf5(argv[i+1]))==0)
        {
            fprintf(stderr,"can't open HDF5 model file %s\n",argv[i+1]);
            exit(1);
        }
    }

    //both formats need the output file (for now)
    output = fopen(argv[i+2],"w");
    if(output == NULL)
    {   
        fprintf(stderr,"can't open output file %s\n",argv[i+2]);
        exit(1);
    }   

    //line might actually not be needed, test to remove it later
    //line = (char *) malloc(max_line_len*sizeof(char));
    x = (Xfloat *) malloc(max_nr_attr*sizeof(Xfloat));
    nz_x = (int *) malloc(max_nr_attr*sizeof(int));

    if(predict_probability)
        if(svm_check_probability_model(model)==0)
        {
            fprintf(stderr,"model does not support probabiliy estimates\n");
            predict_probability=0;
        }

    if (fileformat == 0)
    {
        int l = num_patterns(input);
        //  predict(input,output,l);
        predict_parallel(input, output, l, MPI_COMM_WORLD);
        
        //free(line);
        fclose(input);
    }
    else if (fileformat == 1)
    {
        predict_parallel_hdf5(argv[i], output, MPI_COMM_WORLD);
    }
    else
    {
        exit_with_help();
    }
    
    //common cleanup
    free(x);
    free(nz_x);
    svm_destroy_model(model);
    fclose(output);
    
    //Write out the total execution time
    std::chrono::high_resolution_clock::time_point execution_end = std::chrono::high_resolution_clock::now();
	int executiontime = std::chrono::duration_cast<std::chrono::milliseconds>(execution_end - execution_begin).count();
	fprintf(stdout, "Execution time: %d\n", executiontime);

    MPI_Finalize();
    return 0;
}
